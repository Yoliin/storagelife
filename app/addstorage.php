<script src="https://code.jquery.com/jquery-1.12.1.min.js" integrity="sha256-I1nTg78tSrZev3kjvfdM5A5Ak/blglGzlaZANLPDl3I=" crossorigin="anonymous"></script>
<!-- <script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script> -->
<script>
//Добавление нового значения срока годности товара
	$(document).ready(function() {
		//если выбрали дату в календаре
		$('input[type="date"]').change(function(){
        		var date_useupto = new Date(this.value);
				var d = date_useupto.getDate();
				if (d<10) {d = '0'+d};
				var m =  date_useupto.getMonth();
				m += 1;  // JavaScript months are 0-11
				if (m<10) {m = '0'+m};
				var y = date_useupto.getFullYear();
				var formatted_date = y + "-" + m + "-" + d;//дата приведенная в нужный формат
        		$('#AddStorage').attr('disabled', false);
        		//для передачи даты в другую jQuery функцию, использую глобальное хранилище
        		localStorage.setItem('new_date', formatted_date);

    	});
		//обработчик события щелчка на кнопку Добавить дату
        $("#AddStorage").click(function(event) {
            var id_tovar = $('#id_tovar_temp').val();
            var f_date = localStorage.getItem('new_date');
            //alert(id_tovar+' '+f_date);
			//console.log(f_date);

            console.log(id_tovar+' Date: '+f_date);
            $.ajax({
                url:"app/add_date.php",
                data:{id_tovar:id_tovar, date_useupto:f_date},
                type:"post",
                cache:false,
                success:function(html){
                	//window.location.reload();
                }
            })
            //удаляем:
			localStorage.removeItem("new_date");
			//очищаем все хранилище
			localStorage.clear();
            return false;
        });
    });
</script>

<?php
    include("connect.php");
    //echo "<script>console.log('hello');</script>";
    //echo "<script>console.log({$_POST['barcode']});</script>";
    if (isset($_POST['id_tovar'])){
    	$id_tovar = $_POST['id_tovar'];
    	$q = mysqli_query($con, "Select name from tovar where id_tovar = {$id_tovar}");
        $res = mysqli_fetch_assoc($q);
        echo "<form>";
    	echo "<div><p>Добавить сроки годности к товару <b>{$res['name']}</b> (использовать до)</p></div>";
		echo "
				<div>
				<input id = 'date_useupto' type='date' required>
				<input id = 'AddStorage' type='submit' value='Добавить' disabled>
				<div>
				<table class='table'>
					<thead>
						<tr>
							<th>{$res['name']}</th>
						</tr>
					</thead>
					<tbody>
			";
						//получаем список со сроками годности для выбранного товара
						$query = "SELECT tovar.name, storage.date_useupto as date_useupto FROM tovar, storage where tovar.id_tovar=storage.id_tovar and tovar.id_tovar = {$id_tovar}";
						$q = mysqli_query($con, $query);
            			while ($res = mysqli_fetch_assoc($q))
			            {
			            	//форматируем дату в формат удобный пользователю
			            	$date = $res['date_useupto'];
			            	$formatted_date = date("d.m.Y", strtotime($date));
			            	//выводим дату срока годности в table
			            	echo "<tr><td>{$formatted_date}</td></tr>";
			            };

		echo "
					</tbody>
				</table>
			";

			echo "</div></div>";
			echo "<input id = 'id_tovar_temp' type='text' value='{$id_tovar}' hidden>";
		echo "</form>";
    };
?>
