<script src="https://code.jquery.com/jquery-1.12.1.min.js" integrity="sha256-I1nTg78tSrZev3kjvfdM5A5Ak/blglGzlaZANLPDl3I=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
    	//Добавление нового товара в таблицу
    	//Клик по кнопке Добавить товар
        $("#AddTovar").click(function(event) {
        	if ($('#barcode').val() == '') {
            	var barcode = 'no';}
            else {var barcode = $('#barcode').val();};
            var tovar_name = $('#tovar_name').val();
            var amt = $('#amt').val();
            var id_units = $("#units_name").val();//Получаем значение единиц измерения
            var rate = $('#rate').val();
            var price = $('#price').val();
            console.log("Новый товар: "+barcode+' '+tovar_name+' '+amt+' '+rate+' '+price+' '+id_units);
            $.ajax({
                url:"app/addtovar.php",
                data:{barcode:barcode, tovar_name: tovar_name, amt:amt, rate:rate, price:price, id_units:id_units},
                type:"post",
                cache:false,
                success:function(html){
                	window.location.reload();
                }
            })
            return false;
        });
        //Клик по таблице с товарами
        $(".Tovar_Row").click(function(event) {
        	var id_element = $(this).attr('id');
			var id_tovar = id_element.split('_')[1];
            console.log("товар: "+id_tovar);
            $.ajax({
                url:"app/addstorage.php",
                data:{id_tovar:id_tovar},
                type:"post",
                cache:false,
                success:function(html){
                	$("#date_useupto").html(html);
                }
            })
            return false;
        });
   });
</script>

<?php
  include("app/connect.php");
?>
<!DOCTYPE html>
<html >
<head>
 	<meta charset="UTF-8">
 	<title>Tovar_Storagelife</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/table.css">
</head>
<body>
	<table class="table">
		<thead>
			<tr>
				<th>Номер</th>
				<th>Штрихкод</th>
				<th>Название товара</th>
				<th>Кол-во</th>
				<th>Ед.изм.</th>
				<th>Коэф.</th>
				<th>Цена</th>
				<th>Сумма</th>
			</tr>
		</thead>
		<tbody>
			<?php
				//Заполняем таблицу товаров данными из БД (из view tovar_show)
				$q = mysqli_query($con, "Select * from tovar_show");
                while ($res = mysqli_fetch_assoc($q))
                {
                	echo "
                			<tr>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['id_tovar']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['barcode']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['tovar_name']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['amt']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['units_name']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['rate']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['price']}</td>
                			<td id = 'tovar_{$res['id_tovar']}' class = 'Tovar_Row'>{$res['summ']}</td>
                			</tr>
                		";
                };
			?>
			<?php
					//Строка для добавления данных
                	echo "
                			<tr>
                			<td>+</td>
                			<td><input id = 'barcode' type='text' placeholder='Штрихкод'></td>
                			<td><input id = 'tovar_name' type='text' placeholder='Название'></td>
                			<td><input id = 'amt' type='text' placeholder='Кол-во'></td>
                			<td>
	                			<select id='units_name'>";
	                            $q = mysqli_query($con, "Select * from units");
	                            while ($res = mysqli_fetch_assoc($q))
	                            {
	                              echo "<option value = '{$res['id_units']}'>{$res['name']}</option>";
	                            };
	                            echo "</select>";
	                echo "
	                        </td>

                			<td><input id = 'rate' type='text' placeholder='Коэф.'></td>
                			<td><input id = 'price' type='text' placeholder='Цена'></td>
                			<td><input id = 'AddTovar' type='submit' value='Добавить'></td>
                			</tr>
                		";
			?>
		</tbody>
	</table>
<div id = 'date_useupto'>
</div>
</body>
</html>